package elod.msg;
/**
 * 
 */
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is creating objects containing the machine resources stats. 
 * Each object is holding an object of type Machines with the Machine information and an Object of type Stats, holding the stats of the machine
 * @author ilias
 *
 */
 class MachineStats {
//	private final String cpu;
//	private final String ram;
//	private final String network;
//	private final Date dateTime;
//	private final String ip;
//	private final String title;
	 /**
	  * The Machine Object holding the machine's information
	  */
	private final Machines Machine;
	/**
	 * The statistics of the machine's resources
	 */
	private final Stats Stat;
	
	/**
	 * A list of String containing the machine's data 
	 * @deprecated
	 */
	private final List <String> statInfo=null;
	
//	public MachineStats(List<String> data){
////		this.ip=data.get(0);
////		this.title=data.get(1);
////		this.cpu=data.get(2);
////		this.ram=data.get(3);
////		this.network=data.get(4);
////		this.dateTime=new Date();public	
//	
//		this.statInfo=data;
////		this.statInfo.add(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString());	
//	}
	/**
	 * constructor. An Object of type @see Machines and an Object of type @see Stats
	 * @param machine
	 * @param stat
	 */
	public MachineStats(Machines machine,Stats stat){
		this.Machine=machine;
		this.Stat=stat;
		
	}
	
	/**
	 * returns an Object of type Machines containing the machine's information
	 * @return Machine Object
	 */
	public Machines getMachine(){
		return Machine;
	}
	
	/**
	 * returns an Object of type Stats containing the machine's resources data
	 * @return a Stats object
	 */
	public Stats getStats(){
		return Stat;
	}
	/**
	 * returns a List of string containing the Machine's data.
	 * @see #getMachine()
	 * @see #getStats()
	 * @return List<String>
	 * @deprecated
	 */
	
	public List<String> getInfo(){
		return statInfo;		
	}
	
	/**
	 *  returns the size of the arrayList
	 * @return int
	 * @deprecated
	 */
	public int size(){
		return statInfo.size();
	}
		
	
}

/**
 * This class creates an Object that contains the machine's information
 * @author ilias
 *
 */
 class Machines {
	 /**
	  * The IP of the machine, is used to unique identify the machine
	  */
	private String ip;
	/**
	 * some free text about the machine
	 */
	private String Comment;
	
	/**
	 * Constructor, needs a string for the IP and a String for the free text
	 * @param ip String
	 * @param Comment String
	 */
	public Machines(String ip, String Comment){
		this.ip=ip;
		this.Comment=Comment;		
	}
	/**
	 * Empty Constructor. set methods must be used
	 */
	public Machines(){}
	/**
	 * sets the ip for this Machine Object
	 * @param ip
	 */
	public void setIp(String ip){
		this.ip=ip;
	}
	/**
	 * sets the machine's description for this Machine Object
	 * @param comment
	 */
	public void setComment(String comment){
		this.Comment=comment;
	}
	/**
	 * returns this Objects IP
	 * @return String IP
	 */
	public String getIp(){
		return ip;
	}
	/**
	 * returns this Objects comment
	 * @return String
	 */
	public String getComment(){
		return Comment;
	}

}
 
 /**
  * This clas creates Objects that contain the messages to be sent to the Database
  * @author ilias
  *
  */
  class msgBody{
	  /**
	   * the date-time tag
	   */
		private final String date;
		/**
		 * a String message
		 */
		private final String msg;
		
		/**
		 * Constructor
		 * @param date String
		 * @param msg String
		 */
		public msgBody(String date,String msg){
			this.date=date;
			this.msg=msg;
			
		}
		/**
		 * returns the date-time tag of this Object as a String
		 * @return String Date
		 */
		public String date(){
			return this.date;
		}
		/**
		 * returns the message this Object is holding
		 * @return String message
		 */
		public String message(){
			return this.msg;
		}
	}
  /**
   * This class creates objects that hold the statistics of a machine's resources
   * @author ilias
   *
   */
  class Stats {
	  /**
	   * The cpu speed, is a String representation of the average speed of all the CPU cores
	   */
	  private  String cpu;
	  /**
	   * A String representation of the available Ram out of the total ([available]/[total])
	   */
	  private  String ram;
	  /**
	   * The network speed, [up]/[down]
	   */
	  private  String network;
	  /**
	   * A String representation of the free disk space out of the total space ({free]/[total])
	   */
	  private String hdd;
	  /**
	   * The date_time tag
	   */
	  private  Date dateTime;

	  /**
	   * Constructor gets an array of String and copies each item to its variable. 
	   * The array should be in the format
	   * {[CPU],[RAM],[NetworkSpeed],[Disk]}
	   * @param data a String[] containing the stats of the machine
	   */
	  	public Stats(String[] data){
	  		this.cpu=data[0];
	  		this.ram=data[1];
	  		this.network=data[2];
	  		this.hdd=data[3];
	  		//this.dateTime=new Date();
	  	}
	  	/**
	  	 * get the data for the statistics of this Object and returns them as a String array
	  	 * @return String[]
	  	 */
	  	public String[] getData(){
	  		return new String[]{
	  				cpu,
	  				ram,
	  				network,
	  				hdd
//	  				,				new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString()
	  				};
	  	}
	  	/**
	  	 * returns the cpu data of the Object 
	  	 * @return String
	  	 */
	  	public String getCpu(){
	  		return cpu;
	  	}
	  	/**
	  	 * returns the Ram data of the Object 
	  	 * @return String
	  	 */
	  	public String getRam(){
	  		return ram;		
	  	}
	  	/**
	  	 * returns the network data of the Object 
	  	 * @return String
	  	 */
	  	public String getNetwork(){
	  		return network;
	  	}
	  	/**
	  	 * returns the Hdd data of the Object 
	  	 * @return String
	  	 */
	  	public String getHdd(){
	  		return hdd;
	  	}
	  	
	  }
