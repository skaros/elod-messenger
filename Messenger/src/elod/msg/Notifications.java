package elod.msg;
/*
 /home/ilias/skaros/Dropbox/msgs.csv -u root -p salonika -a jdbc:mysql://127.0.0.1:3306/messenger -as sourceAS1 -at titleAT1 -dn devDN1 -au userAU1
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.sql.PreparedStatement;

import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

public class Notifications {
	public String devName=null;
	public String appName=null;
	public String userName=null;
	public String source=null;
	public String DB_USER=null;
	public String DB_PASS=null;
	public String DB_ADDRS=null;
	//the execution id for the session
	int execId=-1;
	
	List <msgBody> successList=new ArrayList<msgBody>();
	List <msgBody> errorList=new ArrayList<msgBody>();
	List <msgBody> publicList=new ArrayList<msgBody>();
	List <msgBody> warningList=new ArrayList<msgBody>();
	
	//contains information of a machine and the stats of it.
	//the information should be passed in a List of Strings
	List<MachineStats> MachineStats=new ArrayList<MachineStats>();
	
	List<Machines> machines=new ArrayList<Machines>();
	
	/**
	 * Constructor
	 * set up the necessary data prior of inserting to the database
	 * @param sourceName the source of the data
	 * @param user the user
	 * @param app The application that created the message.
	 * @param dev The name of the developer
	 */
	public Notifications(String sourceName,String user,String app,String dev){
		source=sourceName; 
		userName=user;
		devName=dev;
		appName=app;
		
	}
	
	/**
	 * Empty constructor. 
	 * This must be used with the adition of each element individually
	 * i.e. add source, add app etch 
	 */
	public Notifications(){}
	
	public static void main(String[] args) {	

		if(args.length<2){
			//wrong parameters
			System.out.println("Usage ---.jar [pathToMessageFile] [application information] [DB Authentication]");
			System.out.println("\t [pathToMessageFile]: the full path to the file containing the messages to be uploaded");
			System.out.println("\t [application information]...");
			System.out.println("\t [DB Authentication]: the authentication parameters for the Database");
			System.out.println("\t This might be:\n\t\t-f: a file containing the authentication");
			System.out.println("\t OR\n\t\t-u: the username\n\t\t-p: the password\n\t\t-a: the DB address");
			System.exit(-1);
		}
		
		Notifications execute = new Notifications();
				for(int i=1;i<args.length;i=i+2){
					switch(args[i]){
					case "-f":
					case "-F":{
						execute.DbCredentials(new File(args[i+1]));
//						There is a file containning DB authentication
//						DB_USER=new GetCredential(args[i+1]).getProp("Username");
//						DB_PASS=new GetCredential(args[i+1]).getProp("Password");						
//						DB_ADDRS=new GetCredential(args[i+1]).getProp("DataBase");	
						break;
					}
					case "-u":
					case "-U":{
						execute.setDbUser(args[i+1]);
						
//							DB_USER=args[i+1];
							break;
						}
					case "-p":
					case "-P":{
						execute.setDbPass(args[i+1]);
//							DB_PASS=args[i+1];
							break;
						}
					case "-a":
					case "-A":{
						execute.setDbAdrs(args[i+1]);
						break;
						}						
					}//switch
					if(args[i].equalsIgnoreCase("-as")){
						//the source of data for the application						
						execute.setSource(args[i+1]);
//						source=args[i+1];
					}else if(args[i].equalsIgnoreCase("-at")){
						//the application name
						execute.setApp(args[i+1]);
//						appName=args[i+1];
					}else if(args[i].equalsIgnoreCase("-dn")){
						//the developers name
						execute.setDev(args[i+1]);
//						devName=args[i+1];
					}else if(args[i].equalsIgnoreCase("-au")){
						//the user of the application
						execute.setUser(args[i+1]);
//						userName=args[i+1];
					}
					
				}//for
			
		execute.readFile(args[0]);		
	}//main
	/**
	 * take the system resources values and creates a new MachineStats object that is added to the list
	 * @param ip
	 * @param computerTitle
	 * @throws Exception 
	 */
	public void addMachineStat(String ip,String computerTitle) throws Exception{
		Resources rs=new Resources(new Sigar());
		
//		List <Machines> machines=new ArrayList<Machines>();
//		List<Stats> stats =new ArrayList<Stats>();
//		Machines machine=new Machines(ip,computerTitle);
//		//error: network speed change to up/down, add hdd resource on tables
//		Stats stat = null;
		
		try {
			addMachineStat(ip,computerTitle,Double.toString(rs.getAllCpus()),Double.toString(rs.getMemory()),rs.getNetwork(),Double.toString(rs.getFreeHddPerc()));
		} catch (InterruptedException | SigarException e) {
			// TODO Auto-generated catch block
			addMachineStat(ip,computerTitle,null,null,null,null);
			e.printStackTrace();
			throw new Exception("Exception while adding machine stats. Will add null values. Error message:\n"+e.getMessage());
					
		}
		//			stat = new Stats(new String[]{Double.toString(rs.getAllCpus()),Double.toString(rs.getMemory()),rs.getNetwork(),Double.toString(rs.getFreeHddPerc())});
//System.out.println("STATS:"+stat.getCpu()+" "+stat.getHdd()+" "+stat.getRam());

	}
	
	//add a new object of MachineStats to the list.
	public void addMachineStat(String ip,String computerTitle,String cpu,String ram, String ntw,String drive){
		
		List <Machines> machines=new ArrayList<Machines>();
		List<Stats> stats =new ArrayList<Stats>();
		Machines machine=new Machines(ip,computerTitle);
		Stats stat=new Stats(new String[]{cpu,ram,ntw,drive});
		machines.add(machine);
		stats.add(stat);
		
//		List<String> stats=new ArrayList<String>();
//		stats.add(ip);
//		stats.add(computerTitle);
//		stats.add(cpu);
//		stats.add(ram);
//		stats.add(ntw);
		MachineStats.add(new MachineStats(machine,stat));
	}
	

	public void finishExecution(){
		Connection conn;
		try {
			conn = DriverManager.getConnection(DB_ADDRS,DB_USER,DB_PASS);
		//"jdbc:mysql://127.0.0.1:3306/messenger","root","salonika");
			Statement st = conn.createStatement();
		
			 //Source title was not found. insert it
			 st.executeUpdate("update Executions set Finish='"+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString() +"' where Id='"+execId+"';");
			 System.out.println("update Executions set Finish='"+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString() +"' where Id='"+execId+"';");
			 st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
//	
//	//add a new Machine to the list
//	public void addMachine(String ip,String desc){
//		
//	}
	
//	/**
//	 * set a new machine and add it to the list
//	 * @param ip the ip of the machine
//	 * @param title some text to identify the machine
//	 */
//	public void addMachine(String ip, String title){
//		machines.add(new Machines(ip,title));
//	}
	
//	/**
//	 * get the id of the machine with the given data
//	 * @param ip
//	 * @param title
//	 * @return
//	 */
//	public String getMachineId(String ip,String title){
//		//TODO
//		return null;
//	}
	
//	/**
//	 * Add a new row of statistics to the Stats table and return the id 
//	 * @param cpu
//	 * @param ram
//	 * @param network
//	 * @return
//	 */
//	public String addStat(String cpu,String ram,String network){
//		//TODO
//		return null;		
//	}
	
//	/**
//	 * reads the stat from the machine and copy it to the Stats table
//	 * @return
//	 */
//	public String getStat(){
//		//TODO
//		return null;
//	}
	
	
	
	
	
	public  void readFile(String filepath){
		File file=new File(filepath);
		if(file.isFile()){
			String data;
			BufferedReader br = null;
			

			try {
				br = new BufferedReader(new FileReader(file));
				while ((data = br.readLine()) != null) {
					if (data.length()<5)continue ;
					String[] dataArray=data.split("\t");
										
					if(dataArray[0].equalsIgnoreCase("error")){
						errorList.add(new msgBody(dataArray[1],dataArray[2]));
					}else if(dataArray[0].equalsIgnoreCase("public")){
						publicList.add(new msgBody(dataArray[1],dataArray[2]));
					}else if(dataArray[0].equalsIgnoreCase("success")){
						successList.add(new msgBody(dataArray[1],dataArray[2]));
					}else if(dataArray[0].equalsIgnoreCase("warning")){
						warningList.add(new msgBody(dataArray[1],dataArray[2]));
					}
					
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			runInsert();
		}else System.out.println("not a file");
	}
	
	/**
	 * sets the developers name that will be used for the messages
	 * This is the name of the developer of the application that is used
	 * @param dev The name of the developer
	 */
	public void setDev(String dev){
		devName=dev;
	}
	
	/**
	 * sets the name of the application that created the message. 
	 * This should be identical to the other instances, in order to update the database table properly
	 * @param app The application that created the message.
	 */
	public void setApp(String app){
		this.appName=app;
	}
	
	/** 
	 * sets the name of the user that executed the application
	 * This could be empty or null
	 * @param user the user
	 */
	public void setUser(String user){
		this.userName=user;
	}
	
	/**
	 * sets the source that the application is handling. For example it can be "Espa", "Diaygeia" etch.
	 * This should be identical to the other instances, in order to update the database table properly
	 * @param source the source of the data
	 */
	public void setSource(String source){
		this.source=source;
	}
	
	/**
	 * set up the necessary data prior of inserting to the database
	 * @param source the source of the data
	 * @param user the user
	 * @param app The application that created the message.
	 * @param dev The name of the developer
	 */
	public void initialise(String source,String user,String app,String dev){
		this.source=source; 
		this.userName=user;
		this.devName=dev;
		this.appName=app;
	}
	
	/**
	 * set the database authentication parameters
	 * @param user the database username
	 * @param pass the password
	 * @param adrs the database url
	 */
	public void DbCredentials(String user,String pass,String adrs){
		 DB_USER=user;
		 DB_PASS=pass;
		 DB_ADDRS=adrs;
		 //TODO connect to DB
	}
	
	/**
	 * get the file containing the authentication parameters for the db connection
	 * @param credentials the properties file containing the DB authentication
	 */
	public void DbCredentials(File credentials){
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(credentials));
			 DB_PASS= prop.getProperty("Password");
			 DB_USER= prop.getProperty("Username");
			 DB_ADDRS= prop.getProperty("DataBase");
			  
			 if(prop.getProperty("Developer")!=null)
				 devName= prop.getProperty("Developer");
			 if(prop.getProperty("Application")!=null)
				 appName=prop.getProperty("Application");
			 if(prop.getProperty("User")!=null)
				 userName=prop.getProperty("User");
			 if(prop.getProperty("Source")!=null)
				 source=prop.getProperty("Source");
				
				
		} catch (IOException e) {
			e.printStackTrace();					
		}
		 //TODO connect to DB
	}
	
	/**
	 * set the database user name
	 * @param user the database username
	 */
	public void setDbUser(String user){
		 DB_USER=user;
	}
	
	/**
	 * set the database password
	 * @param pass the database password
	 */
	public void setDbPass(String pass){
		 DB_PASS=pass;
	}
	
	/**
	 * set the database address
	 * @param adrs the database address
	 */
	public void setDbAdrs(String adrs){
		 DB_ADDRS=adrs;
	}

	/**
	 * insert the message passed to the database on table Success
	 * returns true if the insert was successful
	 * false if it failed and
	 * throws exception if the needed parameters are missing
	 * @param msg
	 */
	public void addError(String msg) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		errorList.add(new msgBody(dateFormat.format(new Date()),msg));
	}
	
	/**
	 * insert the message passed to the database on table Success
	 * returns true if the insert was successful
	 * false if it failed and
	 * throws exception if the needed parameters are missing
	 * @param msg
	 */
	public void addSuccess(String msg) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		successList.add(new msgBody(dateFormat.format(new Date()),msg));
	}
	
	/**
	 * insert the message passed to the database on table Success
	 * returns true if the insert was successful
	 * false if it failed and
	 * throws exception if the needed parameters are missing
	 * @param msg
	 * @throws Exception 
	 */
	public void addWarning(String msg){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		warningList.add(new msgBody(dateFormat.format(new Date()),msg));
	}
	
	
	/**
	 * insert the message passed to the database on table Public
	 * returns true if the insert was successful
	 * false if it failed and
	 * throws exception if the needed parameters are missing
	 * @param msg
	 * @throws Exception 
	 */
	public void addPublic(String msg){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		publicList.add(new msgBody(dateFormat.format(new Date()),msg));
	}
	
	/**
	 * send ALL the messages passed to the specified recipients, using a gmail account
	 * @param user the username of the gmail account, without the "@gmail.com"
	 * @param password the password of the account
	 * @param recepients a String array containing the recipients.
	 * @param subject the subject of the email
	 */
	public void sendEmail(String user,String password,String[] recepients,String subject){
		
	}
	/**
	 * read the file passed as a parameter, and copy insert the data to the database 
	 * @param file
	 */
	public void save(String file){
		
	}
//	public boolean runInsert(String user,String pass,String adrs){
//		conn = DriverManager.getConnection(user,USER,PASS);
//		st = conn.createStatement();
//		ResultSet rs=null;
//		return true;
//	}
	
	//File credentialsString user,String pass,String adrs
	public String Insert(){
		return runInsert();
	}
	public String runInsert(){
		Connection conn = null;
		
		Statement st = null;
//		
		int sourceId=-1;
		//find the id of the given source
		//if not found insert it
		try {
			conn = DriverManager.getConnection(DB_ADDRS,DB_USER,DB_PASS);//"jdbc:mysql://127.0.0.1:3306/messenger","root","salonika");
		
			st = conn.createStatement();
		ResultSet rs=null;
		rs= st.executeQuery("select Id from Source where Title='"+source+"'");
		 if (rs.isBeforeFirst()){
			 rs.next();
			 sourceId=  rs.getInt("Id");
		 }else{
			 //Source title was not found. insert it
			 st.executeUpdate("INSERT INTO Source (`Title`) VALUES ('"+source+"');");
			
			 st.close();
			 st= conn.createStatement();
			 //now get the id of the inserted source 
			 rs= st.executeQuery("select Id from Source where Title='"+source+"'");
			 rs.next();
			 sourceId= rs.getInt("Id");
			 rs.close();
			 }
		 st.close();
		
		 rs.close();
		 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//find the application id, if already in the table. 
		//if not found insert it and then find it
		int appId=-1;
		try {
			ResultSet rs=null;
			st= conn.createStatement();
			rs= st.executeQuery("select Id from Application where Title='"+appName+"' and Dev_Name='"+devName+"';");
			 if (rs.isBeforeFirst()){
				 //application found. get the id
				 rs.next();
				 appId=  rs.getInt("Id");
			 }
			 else{
				 //application not found, insert it and then find it
				 st.executeUpdate("INSERT INTO Application (`SourceID`,`Title`,`Dev_Name`) VALUES ("+sourceId+",'"+appName+"','"+devName+"')");
				 rs= st.executeQuery("select Id from Application where Title='"+appName+"' and Dev_Name='"+devName+"';");
				 rs.next();
				 appId=  rs.getInt("Id");				 
			 }
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//insert the data to the execution table
		
		try {
			PreparedStatement pstmt =null;
			
			String query="INSERT INTO Executions (`AppId`,`User`,`Start`)VALUES(?,?,?)";//+appId+",'"+userName+"','"+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString()+"');";
			pstmt =
	    		    conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);	
			//+appId+",'"+userName+"','"+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString()+"');";
			pstmt.setLong(1,appId);
			pstmt.setString(2,userName);
			pstmt.setTimestamp(3,new java.sql.Timestamp(System.currentTimeMillis()));
			int affectedRows=pstmt.executeUpdate();
			
			 if (affectedRows == 0) {
				 System.out.println("inserting Execution data failed, no rows affected.");
				 throw new SQLException("inserting Execution data failed, no rows affected.");
			 }
			 //get the new id
			 try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
				 if (generatedKeys.next()) {
					 execId=generatedKeys.getInt(1);
				 }
				 else {
					 System.out.println("inserting Execution data failed, no ID obtained.");
					 throw new SQLException("inserting Execution data failed, no ID obtained.");
				 }
			 }
			
			
			
//			
//			
//			ResultSet rs=null;
//			st= conn.createStatement();
//		
//				st.executeUpdate("INSERT INTO Executions (`AppId`,`User`,`Start`)VALUES("+appId+",'"+userName+"','"+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString()+"');");
//				rs= st.executeQuery("select Id from Executions where AppId='"+appId+"' and User='"+userName+"';");
//				rs.next();
//				execId=  rs.getInt("Id");	
//				System.out.println("EXECUTION ID:"+execId);
//			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//now insert the messages on the appropriate tables
		inserts(conn,"Error",errorList,execId);
		errorList.clear();
		
		inserts(conn,"Public",publicList,execId);
		publicList.clear();
		
		inserts(conn,"Success",successList,execId);
		successList.clear();
		
		inserts(conn,"Warning",warningList,execId);
		warningList.clear();
		
		insertStats(MachineStats,conn,execId);
		MachineStats.clear();
		
		return sourceId+"..";
	}
	
	
	public void insertStats(List<MachineStats> data,Connection conn,int execId){
		Statement st = null;
		
		if (data.size()<1)
			return;
		
		for (MachineStats ms:data){
		PreparedStatement pstmt =null;
		 int machineId=0;
		 int statId=0;
		 String query=null;
			try {
				//try to find the machine id from the machines table
				conn = DriverManager.getConnection(DB_ADDRS,DB_USER,DB_PASS);//"jdbc:mysql://127.0.0.1:3306/messenger","root","salonika");
					st = conn.createStatement();					
				
				ResultSet rs=null;
				query="select Id from Machines where Comment='"+ms.getMachine().getComment()+"' and IP='"+ms.getMachine().getIp()+"'";
				rs= st.executeQuery(query);
				 if (rs.isBeforeFirst()){
					 //machine found
					 rs.next();
					 machineId=  rs.getInt("Id");
				 }else{
					 //machine not found
					 
					 PreparedStatement insert = conn.prepareStatement("INSERT INTO `Machines` (`Ip`,`Comment`) VALUES (?,?);");
					 insert.setString(1,ms.getMachine().getIp() );
					 insert.setString(2,ms.getMachine().getComment());
 
					 //execute the insert
					 insert.executeUpdate();
					 insert.close();
					 
					 //get the machine id
					ResultSet machineIdResult= st.executeQuery(query);
					machineIdResult.next();
					 machineId=  machineIdResult.getInt("Id");					
					 machineIdResult.close();
					 
				 }//else
				rs.close();
			
				//now insert the stats
				String[] statData=ms.getStats().getData(); 
				query="INSERT INTO `Machine_Stats` (`Cpu`,`Ram`,`Network`,`Hdd`) VALUES (?,?,?,?);";
				pstmt =
		    		    conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);	
			
				for(int i=0;i<statData.length;i++){
					pstmt.setString(i+1,statData[i]);
				}
				int affectedRows=pstmt.executeUpdate();
				
				 if (affectedRows == 0) {
					 System.out.println("inserting stats data failed, no rows affected.");
					 throw new SQLException("inserting stats data failed, no rows affected.");
				 }
				 //get the new id
				 try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
					 if (generatedKeys.next()) {
						 statId=generatedKeys.getInt(1);
					 }
					 else {
						 System.out.println("inserting stats data failed, no ID obtained.");
						 throw new SQLException("inserting stats data failed, no ID obtained.");
					 }
				 }
				 pstmt.close();
				 
				 //insert to the Exec_Stats table
				 pstmt =
			    		    conn.prepareStatement("INSERT INTO `Exec_Stats` (`ExecId`,`MachineId`,`StatsId`,`Date_Time`) VALUES (?,?,?,?);");
				 pstmt.setInt(1,execId);
				 pstmt.setInt(2,machineId);
				 pstmt.setInt(3,statId);
				 pstmt.setString(4,new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString());
				pstmt.executeUpdate();
				 pstmt.close();

			} catch (SQLException e) {
				System.out.println("EXCEPTION on Insert into Machines, Stats and MachineStats"+e.getLocalizedMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}//for
	}
		
	public void inserts(Connection conn,String table,List<msgBody> data,int execId){
		if (data.size()<1)
			return;
		PreparedStatement pstmt =null;
		try {
			pstmt =
	    		    conn.prepareStatement("INSERT INTO "+table+" (`ExecId`, `Message`,`Date_Time`)VALUES(?,?,?);");
			pstmt.setInt(1,execId);
		for (int i=0;i<data.size();i++){
				pstmt.setString(2,data.get(i).message());
//				pstmt.setString(3,data.get(i).date());	
			pstmt.execute();			
			}
		} catch (SQLException e) {
			System.out.println("EXCEPTION"+data.get(0).date());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean isEmpty(){
		if(errorList==null&&publicList==null&&successList==null&&warningList==null)
			return true;
		else 
			return false;
	}

}
