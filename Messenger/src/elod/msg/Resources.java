package elod.msg;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.FileSystemUsage;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.NetFlags;
import org.hyperic.sigar.NetInterfaceConfig;
import org.hyperic.sigar.NetInterfaceStat;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

public class Resources {
	Sigar sigar;
	public Resources(Sigar s){
	sigar=s;
}
	/**
	 * reads the cpu load over a period of 3 seconds and returns the average for all the cores
	 * @return the average for all the cores or -1 on error
	 * @throws SigarException
	 * @throws InterruptedException 
	 */
	public double getAllCpus() throws  InterruptedException{
		CpuPerc[] cpuPercs;
		try {
			cpuPercs = sigar.getCpuPercList();
			double percent=0.0;
			for(int count=0;count<3;count++){
				for (CpuPerc cpuPerc : cpuPercs) {			
					percent+=cpuPerc.getCombined();				
				}
				Thread.sleep(1000);
			}
			return percent/(cpuPercs.length);
		} catch (SigarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1d;
		}
		
	}
	
	/**
	 * Uses the third party software from class NetworkData and returns an array of double that 
	 * the first element is the download speed and the second element the Upload speed, both in Bytes/Sec
	 * @return String
	 */
	public String getNetwork(){
		int count=0;
		while (true){
			try {
				Thread.sleep(1000);
				return new NetworkData(sigar).newMetricThread();
			} catch (SigarException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				count++;
				if (count<=5){					
					continue;
				}else
				return null;
		}
		}
	}
	/**
	 * gets the used ram, divide it with the total available ram and returns the persentage of used memory
	 * @return % of used ram
	 * @throws SigarException
	 */
	public double getMemory() throws SigarException{
		Mem mem   =sigar.getMem();	
		return sigar.getMem().getActualUsed()*100/mem.getTotal();
	}
	
	/**
	 * gets the free space of the root drive and returns it as a percentage of the total drive space
	 * @return double, the percentage of free disk space
	 * @throws SigarException
	 */
	public double getFreeHddPerc() throws SigarException{
		File[] roots = File.listRoots();
		
		FileSystemUsage f= sigar.getFileSystemUsage("/");//roots[0].getAbsolutePath());
	
		return ( f.getAvail()*100.0/f.getTotal() );  
		
	}
}

class NetworkData {
	static Map<String, Long> rxCurrentMap = new HashMap<String, Long>();
    static Map<String, List<Long>> rxChangeMap = new HashMap<String, List<Long>>();
    static Map<String, Long> txCurrentMap = new HashMap<String, Long>();
    static Map<String, List<Long>> txChangeMap = new HashMap<String, List<Long>>();
    private static Sigar sigar;
 
    public NetworkData(Sigar s) throws SigarException, InterruptedException {
      sigar = s;
      }
    
    /**
     * 
     * @return an array of double, the first element is the download speed and the second element the Upload speed, both in Bytes/Sec 
     * @throws SigarException
     * @throws InterruptedException
     */
        public String newMetricThread() throws SigarException, InterruptedException {
            double avgUp=0,avgDown=0;
           
            for (int i=0;i<3;i++){
            	Long[] m = getMetric();
                avgDown+= m[0];
                avgUp += m[1];
                Thread.sleep(1000);
            }
            return avgDown/3+"/"+avgUp/3;
        }
        
        public static Long[] getMetric() throws SigarException {
            for (String netInterface : sigar.getNetInterfaceList()) {
             
                NetInterfaceStat netStat = sigar.getNetInterfaceStat(netInterface);
                NetInterfaceConfig ifConfig = sigar.getNetInterfaceConfig(netInterface);
                String hwaddr = null;
                if (!NetFlags.NULL_HWADDR.equals(ifConfig.getHwaddr())) {
                    hwaddr = ifConfig.getHwaddr();
                }
                if (hwaddr != null) {
                    long rxCurrenttmp = netStat.getRxBytes();
                    saveChange(rxCurrentMap, rxChangeMap, hwaddr, rxCurrenttmp, netInterface);
                    long txCurrenttmp = netStat.getTxBytes();
                    saveChange(txCurrentMap, txChangeMap, hwaddr, txCurrenttmp, netInterface);
                }
            }
            long totalrx = getMetricData(rxChangeMap);
            long totaltx = getMetricData(txChangeMap);
            for (List<Long> l : rxChangeMap.values())
                l.clear();
            for (List<Long> l : txChangeMap.values())
                l.clear();
            return new Long[] { totalrx, totaltx };
        }

        private static long getMetricData(Map<String, List<Long>> rxChangeMap) {
            long total = 0;
            for (Entry<String, List<Long>> entry : rxChangeMap.entrySet()) {
                int average = 0;
                for (Long l : entry.getValue()) {
                    average += l;
                }
                total += average / entry.getValue().size();
            }
            return total;
        }

        private static void saveChange(Map<String, Long> currentMap,
                Map<String, List<Long>> changeMap, String hwaddr, long current,
                String ni) {
            Long oldCurrent = currentMap.get(ni);
            if (oldCurrent != null) {
                List<Long> list = changeMap.get(hwaddr);
                if (list == null) {
                    list = new LinkedList<Long>();
                    changeMap.put(hwaddr, list);
                }
                list.add((current - oldCurrent));
            }
            currentMap.put(ni, current);
        }
}
